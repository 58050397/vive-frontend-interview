import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    goodsData : [],
    order : [],
    totalPrice: 0
  },
  mutations: {
    ADD_ORDERLIST( state, goodsOrder ){
        if( state.order.length == 0 ){
            let feeObject = {
                id: 'fee',
                name: 'Fee',
                price: 10
            }
            state.order.push( feeObject )
        }
        let hasInOrder = false
        for( let index = 0; index < state.order.length; ++index ){
            if( state.order[index].id == goodsOrder.id ){
                state.order[index].price += state.order[index].price/state.order[index].amount
                ++state.order[index].amount
                hasInOrder = true
            }
        }
		if( !hasInOrder ){
            let orderObject = {
                id : goodsOrder.id,
                name: goodsOrder.name,
                amount: 1,
                price: goodsOrder.price
            }
            state.order.push( orderObject )
        }
        state.totalPrice += goodsOrder.price

        if( state.totalPrice > 100 ){
            state.order.forEach( item => {
                if( item.id == 'fee' ){
                    item.price = 0
                }
            })
        }
    },
    REMOVE_ORDERLIST( state, goodsId ){
        state.order.forEach( (item,index) => {
            if( item.id == goodsId ){
                if( item.amount == 1 ) {
                    state.totalPrice -= item.price
                    state.order.splice(index,1)
                }
                else {
                    state.totalPrice -= item.price/item.amount
                    item.price -= item.price/item.amount
                    --item.amount 
                }
            }
        })

        if( state.totalPrice <= 100 ){
            state.order.forEach( item => {
                if( item.id == 'fee' ){
                    item.price = 10
                }
            })
        }

        if( state.order.length == 1 ){
            state.order.pop()
            state.totalPrice = 0
        }

    }
  },
  actions: {
    addOrderList( {commit}, goodsOrder ){
		commit( 'ADD_ORDERLIST', goodsOrder )
    },
    removeOrderList( {commit}, goodsId ){
        commit( 'REMOVE_ORDERLIST', goodsId )
    }
  }
})
