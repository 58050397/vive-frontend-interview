# vive-frontend-interview that implemented by Sahaparb Srisawangnet

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Run Node server for provide data
```
node server.js
```

### Compiles and minifies for production
```
npm run build
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### main page
See http://localhost:8080/